<?php
interface Formatter
/*Интерфейс для имплементации различных вариантов форматов сообщений*/
{
    public function format();
}
?>