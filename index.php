<?php
include_once(dirname(__FILE__) . '/bootstrap.php');

$emailDate = new EmailDeliveryWithDate('message');
$smsRaw = new SmsDeliveryRaw('second message');
$consoleDetails = new ConsoleDeliveryDetails('third message', 15);

$logger = new Logger();
$logger->log($emailDate);
$logger->log($smsRaw);
$logger->log($consoleDetails);

$logger->getArchieve();
?>
