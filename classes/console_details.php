<?php
class ConsoleDeliveryDetails implements Vendor, Formatter
{
    private $string;
    private $id;

    public function __construct($string, $id) {
        $this->string = $string;
        $this->id = $id;
    }

    public function format() {
        return $this->format = date('Y-m-d H:i:s ') . $this->string . ' with ID: ' . $this->id;
    }

    public function deliver() {
        return "Вывод формата ({$this->format()}) в консоль";
    }
}
?>