<?php
class Logger
{
    private $archieve = [];                          //Создаем архив приходящих логов в виде массива

    public function log(Vendor $source) {            //Функция логирования
        echo 'Лог успешно добавлен';
        echo '<br>';
        $this->archieve[] = $source->deliver();      //Добавляем приходящий лог доставки в архив
    }

    public function getArchieve() {                  //Вывод архива всех логов доставки
        foreach ($this->archieve as $log) {
            echo $log;
            echo '<br>';
        }
    }
}
?>