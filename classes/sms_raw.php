<?php
class SmsDeliveryRaw implements Vendor, Formatter
{
    private $string;

    public function __construct($string) {
        $this->string = $string;
    }

    public function format() {
        return $this->string;
    }

    public function deliver() {
        return "Вывод формата ({$this->format()}) в смс";
    }
}
?>