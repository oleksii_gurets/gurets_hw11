<?php
class EmailDeliveryWithDate implements Vendor, Formatter
{
    private $string;

    public function __construct($string) {
        $this->string = $string;
    }

    public function format() {
        return $this->format = date('Y-m-d H:i:s ') . $this->string;
    }

    public function deliver() {
        return "Вывод формата ({$this->format()}) по имейл";
    }
}
?>